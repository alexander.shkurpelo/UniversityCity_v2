#pragma once
#include <unordered_set>
#include "DepartmentSet.h"
#include "HostelSet.h"

using namespace std;

namespace UniversityCity
{
	class UniversityCityData : public DepartmentSet, public HostelSet
	{
	protected:
		typedef weak_ptr<Department> DepartmentWPtr;
		typedef weak_ptr<Student> StudentWPtr;
		typedef shared_ptr<Student> StudentSPtr;
		typedef unordered_set<StudentWPtr, WeakPtrHash<Student>, WeakPtrEqual<Student>> StudentsLink;
	public:
		UniversityCityData();
		~UniversityCityData();

		StudentWPtr getStudentByName(const string& name) const;
		StudentsLink getUniversityStudents() const;
		StudentsLink getUniversityStudentsWithFacilities() const;
		StudentsLink getUniversityStudentsWithoutFacilities() const;
	};

}

