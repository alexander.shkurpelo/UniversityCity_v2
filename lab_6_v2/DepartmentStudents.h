#pragma once
#include <memory>
#include <unordered_set>
#include "WeakPtrUtility.h"
#include "Student.h"

using namespace std;

namespace UniversityCity
{
	class DepartmentStudents
	{
	protected:
		typedef shared_ptr<Student> StudentSPtr;
		typedef weak_ptr<Student> StudentWPtr;
		typedef unordered_set<StudentSPtr> Students;
		typedef unordered_set<StudentWPtr, WeakPtrHash<Student>, WeakPtrEqual<Student>> StudentsLink;

		Students students_;
	public:
		DepartmentStudents();
		virtual ~DepartmentStudents();

		virtual bool addStudent(const StudentSPtr& student);
		void addStudents(const Students& students);
		StudentSPtr removeStudent(const StudentWPtr& student);
		Students removeAllStudents();

		StudentWPtr getStudentByName(const string& name) const;
		StudentsLink getStudents() const;
		StudentsLink getStudentsWithFacilities() const;
		StudentsLink getStudentsWithoutFacilities() const;
	};
}
