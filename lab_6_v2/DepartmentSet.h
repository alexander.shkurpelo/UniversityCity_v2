#pragma once
#include "Department.h"

using namespace std;

namespace UniversityCity
{
	class DepartmentSet
	{
	protected:
		typedef shared_ptr<Department> DepartmentSPtr;
		typedef unordered_set<DepartmentSPtr> Departments;
		
		Departments departments_;
	public:
		DepartmentSet();
		virtual ~DepartmentSet();

		bool addDepartment(const DepartmentSPtr& department);
		void addDepartments(const Departments& departments);

		DepartmentSPtr getDepartmentByName(const string& name) const;
		Departments getDepartments() const;

		DepartmentSPtr removeDepartment(const DepartmentSPtr& department);
		DepartmentSPtr removeDepartmentByName(const string& name);
		Departments removeAllDepartments();
	};
}
