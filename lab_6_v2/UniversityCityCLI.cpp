#include "stdafx.h"
#include <iostream>
#include <random>
#include "UniversityCityCLI.h"
#include "Student.h"
#include <ctime>
#include <map>
#include <queue>

namespace UniversityCity
{
	UniversityCityCLI::UniversityCityCLI() : city(UniversityCity())
	{
		cityInfo = city.getInfoClass();
		state = Intro;
		initStrings();
	}

	UniversityCityCLI::~UniversityCityCLI()
	{
	}

	void UniversityCityCLI::screenManager()
	{
		while (true)
		{
			switch (state)
			{
			case (Intro):
			{
				introScreen();
				break;
			}
			case (Main):
			{
				mainMenuScreen();
				break;
			}
			case (AddDepartment):
			{
				addDepartmentScreen();
				break;
			}
			case (AddedDepartment):
			{
				addedDepartmentScreen();
				break;
			}
			case (ChoseDepartment):
			{
				choseDepartmentScreen();
				break;
			}
			case (ChosenDepartment):
			{
				chosenDepartmentScreen();
				break;
			}
			case (ChoseHostel):
				{
				choseHostelScreen();
				break;
				}
			case (ChosenHostel):
				{
				chosenHostelScreen();
				break;
				}
			case (ChoseStudent):
				{
				choseStudentScreen();
				break;
				}
			case (ChosenStudent):
				{
				chosenStudentScreen();
				break;
				}
			case (ManageDepartment):
				{
				manageDepartmentScreen();
				break;
				}
			case (AddStudent):
				{
				addStudentScreen();
				break;
				}
			case (AddHostel):
				{
				addHostelScreen();
				break;
				}
			case (InfoDepartment):
				{
				infoDepartmentScreen();
				break;
				}
			case (InfoHostel):
				{
				infoHostelScreen();
				break;
				}
			case (Info):
				{
				infoScreen();//add method
				break;
				}
			case (MakeSettlement):
				{
				makeSettlement();
				break;
				}
			case (Exit):
				{
				cityInfo.reset();
				return;
				}

			}
		}
	}

	void UniversityCityCLI::initStrings()
	{
		string delimeter = "\n" + string(100, '-') + "\n";
		strings = unordered_map<string, string>();
		strings["delim"] = delimeter;
		strings["Main"] = delimeter + getOperationString({ "Add department", "Chose department", "Chose hostel", "Chose student", "Info", "Make settlement", "Exit" });
		strings["Command"] = "\ncommand:";
		strings["WrongCommand"] = "Wrong command\n";
		strings["AddDepartemnt"] = delimeter + "Input department's name: ";
		strings["AddDepartmentSucces"] = "Departmetn %s succesfully added.";
		strings["AddDepartmentFailure"] = "Department %s already exists";
	}

	void UniversityCityCLI::printDepartments()
	{
		auto departments = city.getDepartments();
		printf(strings["delim"].c_str());
		printf("Departments (%d):\n", departments.size());
		for (auto const department : departments)
		{
			printf("\t%s\n", department->getName().c_str());
		}
	}

	void UniversityCityCLI::printHostels()
	{
		auto hostels = city.getHostels();
		printf(strings["delim"].c_str());
		printf("Hostels (%d):\n", hostels.size());
		for (auto const hostel : hostels)
		{
			printf("\t%u\n", hostel->getNumber());
		}
	}

	void UniversityCityCLI::introScreen()
	{
		cout << "\n\t\t\t\t     Assignment #6-7, level C\n\t\t\tStudents settlement by Alexander Shkurpelo\n\n\n";
		printf(strings["delim"].c_str());
		printf("Fill data with true random values?");
		printf(getOperationString({ "Yes", "No" }).c_str());
		switch (menuChose(2))
		{
		case (1):
			{
			fillRandomData();
			printf("\nDone.\n\n");
			break;
			}
		case (2):
			{
			printf("\n\n");
			break;
			}
		}
		state = Main;
	}

	void UniversityCityCLI::makeSettlement()
	{
		printf("\n\nSettlementing...");
		city.organizeStudents();
		printf("done.\n\n");
		state = Main;
	}

	void UniversityCityCLI::mainMenuScreen()
	{
		printDepartments();
		printHostels();
		printf(strings["Main"].c_str());
		string chosenMatrix;
		state = menuChose({AddDepartment, ChoseDepartment, ChoseHostel, ChoseStudent, Info, MakeSettlement, Exit});
	}

	void UniversityCityCLI::addDepartmentScreen()
	{
		printf(strings["AddDepartemnt"].c_str());
		string name;
		getline(cin, name);
		getline(cin, name);
		if (city.addDepartment(make_shared<Department>(name)))
		{				
			printf(strings["AddDepartmentSucces"].c_str(), name.c_str());
			curDepartmentName = name;
			state = AddedDepartment;
			return;
		}
		printf(strings["AddDepartmentFailure"].c_str(), name.c_str());
		printf("\n\nTry again?\n    1 - Yes\n    2 - No\n");
		state = menuChose({ AddDepartment, Main });
	}

	void UniversityCityCLI::addedDepartmentScreen()
	{
		printf(strings["delim"].c_str());
		printf("Chose added department?\n    1 - Yes\n    2 - No\n");
		state = menuChose({ ChosenDepartment, Main });
		if (state == Main)
			curDepartmentName = "";
	}

	void UniversityCityCLI::choseDepartmentScreen()
	{
		printf(strings["delim"].c_str());
		printf("Input department's name:");
		string name;
		getline(cin, name);
		getline(cin, name);
		if (city.getDepartmentByName(name) != nullptr)
		{
			curDepartmentName = name;
			state = ChosenDepartment;
			return;
		}
		printf("There is no department with name %s", name.c_str());
		printf("\n\nTry again?\n    1 - Yes\n    2 - No\n");
		state = menuChose({ ChoseDepartment, Main });
	}

	void UniversityCityCLI::printDepartmentHeaderInfo()
	{
		printf("%sChosen department - %s ", strings["delim"].c_str(), curDepartmentName.c_str());
		printf("( %d hostels, %d students )\n", cityInfo.hostelsCount(curDepartmentName), cityInfo.allStudentsCount(curDepartmentName));
	}

	void UniversityCityCLI::chosenDepartmentScreen()
	{
		printDepartmentHeaderInfo();
		printf(getOperationString({ "Manage", "Info", "Back" }).c_str());
		state = menuChose({ ManageDepartment, InfoDepartment, Main });
		if (state == Main)
			curDepartmentName = "";
	}

	void UniversityCityCLI::manageDepartmentScreen()
	{
		printDepartmentHeaderInfo();
		printf(getOperationString({"Add student", "Add hostel", "Chose student", "Chose hostel", "Delete", "Back" }).c_str());
		state = menuChose({ AddStudent, AddHostel, ChoseStudent, ChoseHostel, DeleteDepartment, ChosenDepartment });
	}

	void UniversityCityCLI::infoDepartmentScreen()
	{
		printDepartmentHeaderInfo();
		printf(getOperationString({ "Total info", "Students list", "Hostels list", "Back" }).c_str());
		auto dep = city.getDepartmentByName(curDepartmentName);
		auto chose = menuChose(4);
		switch (chose)
		{
		case (1):
			{
			auto studentsWithFacilitiesCount = dep->getStudentsWithFacilities().size();
			auto studentsWithoutFacilitiesCount = dep->getStudentsWithoutFacilities().size();
			auto settlementStudentsCount = dep->getStudents().size();
			auto studentsInQueue = city.getStudentsQueue().getDepartmentQueue(dep).size();
			printf("\tStudents with facilities: %d\n\tStudents without facilities: %d\n\tSettlement students: %d\n\tStudents in queue: %d\n",
					studentsWithFacilitiesCount, studentsWithoutFacilitiesCount, settlementStudentsCount, studentsInQueue);
			system("pause");
			break;
			}
		case (2):
		{
			printf("\nStudents list:\n");
			printf(getOperationString({ "All students", "Students with facilities", "Students without facilities", "Back"}).c_str());
			switch (menuChose(4))
			{
			case (1):
			{
				printf("\nAll students (%d):\n", dep->getStudents().size());
				for (auto student : dep->getStudents())
				{
					if (auto st = student.lock())
						printf("\t%s\t\t%s\n", st->getDepartment().lock()->getName().c_str(), st->getName().c_str());
				}
				system("pause");
				break;
			}
			case (2):
			{
				printf("\nStudents with facilities (%d):\n", dep->getStudentsWithFacilities().size());
				for (auto student : dep->getStudentsWithFacilities())
				{
					if (auto st = student.lock())
						printf("\t%s\t\t%s\n", st->getDepartment().lock()->getName().c_str(), st->getName().c_str());
				}
				system("pause");
				break;
			}
			case (3):
			{
				printf("\nStudents without facilities (%d):\n", dep->getStudentsWithoutFacilities().size());
				for (auto student : dep->getStudentsWithoutFacilities())
				{
					if (auto st = student.lock())
						printf("\t%s\t\t%s\n", st->getDepartment().lock()->getName().c_str(), st->getName().c_str());
				}
				system("pause");
				break;
			}
			case (4):
			{
				state = InfoDepartment;
				break;
			}
			}
			break;
		}
		case (3):
			{
			printf("\nHostels list:\n");
			printDepartmentHostels();
			system("pause");
			break;
			}
		case (4):
			{
			state = ChosenDepartment;
			}
		}
	}

	void UniversityCityCLI::addStudentScreen()
	{
		printf("%sInput student`s name: ", strings["delim"].c_str());
		string name;
		getline(cin, name);
		getline(cin, name);
		printf("\nHas facilities:\n");
		printf(getOperationString({ "Yes", "No" }).c_str());
		bool hasFacilities = menuChose(2) == 1;
		if (!city.getDepartmentByName(curDepartmentName)->getStudentByName(name).expired())
		{
			printf("\nStudent with such name already exists in department %s\nContinue?\n", curDepartmentName.c_str());
			printf(getOperationString({ "Yes", "No" }, false).c_str());
			bool cont = menuChose(2) == 1;
			if (!cont)
			{
				printf("\n\nTry again?\n    1 - Yes\n    2 - No\n");
				state = menuChose({ AddStudent, ManageDepartment });
				return;
			}
		}
		city.getDepartmentByName(curDepartmentName)->addStudent(make_shared<Student>(name, hasFacilities));
		printf("\n\nStudent added succesfully.");
		state = ManageDepartment;
		return;
	}

	void UniversityCityCLI::addHostelScreen()
	{
		printf("%sInput hostels's number: ", strings["delim"].c_str());
		int num;
		try
		{
			cin >> num;
			if (num < 0)
				throw exception();
		}
		catch(...)
		{
			printf("\n\nWrong number.\nRepeat?\n    1 - Yes\n    2 - No");
			state = menuChose({ AddHostel, ManageDepartment });
			return;
		}
		if (!city.getDepartmentByName(curDepartmentName)->getHostelByNumber(num).expired())
		{
			printf("\n\nHostel with such number already exists in department.\nRepeat?\n    1 - Yes\n    2 - No");
			state = menuChose({ AddHostel, ManageDepartment });
			return;
		}
		if (city.getHostelByNumber(num) != nullptr)
		{
			auto hostel = city.getHostelByNumber(num);
			hostel->addDepartment(city.getDepartmentByName(curDepartmentName));
			city.getDepartmentByName(curDepartmentName)->addHostel(hostel);
		}
		else
		{
			int allPlaces, availablePlaces;
			try
			{
				printf("\nInput hostel's all places:");
				cin >> allPlaces;
				printf("\nInput hostel's available places:");
				cin >> availablePlaces;
				if (allPlaces < availablePlaces || allPlaces <= 0 || availablePlaces < 0)
					throw exception();
			}
			catch (...)
			{
				printf("\n\nWrong places number.\nRepeat?\n    1 - Yes\n    2 - No");
				state = menuChose({ AddHostel, ManageDepartment });
				return;
			}
			auto hostel = make_shared<Hostel>(num, allPlaces, availablePlaces);
			city.addHostel(hostel);
			city.getDepartmentByName(curDepartmentName)->addHostel(hostel);
		}
		printf("\nHostel succesfully added.");
		state = ManageDepartment;
	}

	void UniversityCityCLI::addedHostelScreen()
	{

	}

	void UniversityCityCLI::choseStudentScreen()
	{
		printf("%sInput student`s name: ", strings["delim"].c_str());
		string name;
		getline(cin, name);
		getline(cin, name);
		if (curDepartmentName != "")
		{
			if (city.getDepartmentByName(curDepartmentName)->getStudentByName(name).expired())
			{
				printf("\n\nWhere is no students with such name in department %s\n", curDepartmentName.c_str());
				printf("\nTry again?\n    1 - Yes\n    2 - No\n");
				state = menuChose({ ChoseStudent, ManageDepartment });
				return;
			}
		}
		else
		{
			if (city.getStudentByName(name).expired())
			{
				printf("\n\nWhere is no students with such name in university\n");
				printf("\nTry again?\n    1 - Yes\n    2 - No\n");
				state = menuChose({ ChoseStudent, Main });
				return;
			}
		}
		curStudentName = name;
		state = ChosenStudent;
	}

	void UniversityCityCLI::choseHostelScreen()
	{
		printf("%sInput hostel`s number: ", strings["delim"].c_str());
		int num;
		try
		{
			cin >> num;
			if (num < 0)
				throw exception();
		}
		catch (...)
		{
			printf("\n\nWrong number\n");
			printf("\nTry again?\n    1 - Yes\n    2 - No\n");
			state = menuChose({ ChoseHostel, curDepartmentName == "" ? Main : ManageDepartment });
			return;
		}
		if (curDepartmentName != "")
		{
			if (city.getDepartmentByName(curDepartmentName)->getHostelByNumber(num).expired())
			{
				printf("\n\nWhere is no hostel with such number in department %s\n", curDepartmentName.c_str());
				printf("\nTry again?\n    1 - Yes\n    2 - No\n");
				state = menuChose({ ChoseHostel, ManageDepartment });
				return;
			}
		}
		else
		{
			if (city.getHostelByNumber(num) == nullptr)
			{
				printf("\n\nWhere is no hostel with such number in university\n");
				printf("\nTry again?\n    1 - Yes\n    2 - No\n");
				state = menuChose({ ChoseHostel, Main });
				return;
			}
		}
		curHostelNumber = num;
		state = ChosenHostel;
	}

	void UniversityCityCLI::chosenHostelScreen()
	{
		printf(getOperationString({ "Info", "Remove from department", "Remove from university", "Back" }).c_str());
		int chose = menuChose(4);
		switch (chose)
		{
		case (1):
		{
			state = InfoHostel;
			return;
		}
		case (2):
		{
				if (curDepartmentName != "")
				{
					city.getDepartmentByName(curDepartmentName)->removeHostel(city.getHostelByNumber(curHostelNumber));
				}
				else
				{
					printf("\n\nInput deparment's name: ");
					string name;
					cin >> name;
					if (city.getDepartmentByName(name) == nullptr)
					{
						printf("\n\nThere is no departmnent with such name");
						return;
					}
					if (auto hos = city.getDepartmentByName(name)->getHostelByNumber(curHostelNumber).lock())
					{
						hos->removeDepartment(city.getDepartmentByName(name));
						printf("\n\nDone.");
						return;
					}
				}
		}
		case (3):
		{
			city.removeHostelByNumber(curHostelNumber);
			curHostelNumber = -1;
			printf("\n\nDone.");
			state = curDepartmentName == "" ? Main : ManageDepartment;
			return;
		}
		case (4):
		{
			state = curDepartmentName == "" ? Main : ManageDepartment;
			return;
		}
		}
	}

	void UniversityCityCLI::infoHostelScreen()
	{
		printf(strings["delim"].c_str());
		printf("Chosen hostel %u\n", curHostelNumber);
		printf(getOperationString({ "Total info", "Students list", "Departments list", "Back" }).c_str());
		auto hostel = city.getHostelByNumber(curHostelNumber);
		int chose = menuChose(4);
		switch (chose)
		{
		case (1):
		{
			printf("\nTotal info:\n");
			printf("  All places:%d;\tAvailable places:%d;\tSettlemented students:%d\n", hostel->getAllPlaces(), hostel->getAvailablePlaces(), hostel->getStudents().size());
			printf("  Students with facilities: %d;\tStudents without facilities: %d\n", hostel->getStudentsWithFacilities().size(), hostel->getStudentsWithoutFacilities().size());
			system("pause");
			break;
		}
		case (2):
			{
			printf("\nStudents list:\n");
			printf(getOperationString({ "All students", "Students with facilities", "Students without facilities", "Back"}).c_str());
				switch (menuChose(3))
				{
				case (1):
					{
					printf("\nAll students (%d):\n", hostel->getStudents().size());
					for (auto student : hostel->getStudents())
					{
						if (auto st = student.lock())
							printf("\t%s\t\t%s\n", st->getDepartment().lock()->getName().c_str(), st->getName().c_str());
					}
					break;
					}
				case (2):
					{
					printf("\nStudents with facilities (%d):\n", hostel->getStudentsWithFacilities().size());
						for (auto student : hostel->getStudentsWithFacilities())
						{
							if (auto st = student.lock())
								printf("\t%s\t\t%s\n", st->getDepartment().lock()->getName().c_str(), st->getName().c_str());							
						}
						break;
					}
				case (3):
				{
					printf("\nStudents without facilities (%d):\n", hostel->getStudentsWithoutFacilities().size());
					for (auto student : hostel->getStudentsWithoutFacilities())
					{
						if (auto st = student.lock())
							printf("\t%s\t\t%s\n", st->getDepartment().lock()->getName().c_str(), st->getName().c_str());
					}
					break;
				}
				case (4):
					{
					state = curDepartmentName == "" ? Main : ManageDepartment;
					break;
					}
				}
				system("pause");
				break;
			}
		case (3):
			{
			auto departments = hostel->getDepartments();
			printf("\nDepartments:\n");
				for (auto departmentW : departments)
				{
					if (auto department = departmentW.lock())
						printf("\t%s - %d students in this hostel\n", department->getName().c_str(), hostel->getStudentsByDepartmentName(department->getName()).size());
				}
				system("pause");
			}
		case (4):
			{
			state = ChosenHostel;
			break;
			}
		}
	}

	void UniversityCityCLI::chosenStudentScreen()
	{
		printf(getOperationString({ "Info", "Remove from hostel", "Remove from university", "Back" }).c_str());
		int chose = menuChose(4);
		switch (chose)
		{
		case (1):
		{
			printStudentInfo(curStudentName);
			return;
		}
		case (2):
		{
			auto student = city.getStudentByName(curStudentName).lock();
			if (student->getHostel().expired())
			{
				printf("\n\nStudent don't live in hostel\n");
				return;
			}
			student->getHostel().lock()->removeStudent(student);
			curStudentName = "";
			state = ManageDepartment;
			break;
		}
		case (3):
		{
			auto student = city.getStudentByName(curStudentName).lock();
			student->getDepartment().lock()->removeStudent(student);
			state = ManageDepartment;
			curStudentName = "";
			break;
		}
		case (4):
		{
			state = curDepartmentName == "" ? Main : ManageDepartment;
			curStudentName = "";
			return;
		}
		}
		printf("\nDone.\n");
	}

	void UniversityCityCLI::infoScreen()
	{
		printf(strings["delim"].c_str());
		printf(getOperationString({ "General settlement info", "Students list", "Back" }).c_str());
		switch(menuChose(3))
		{
		case (1):
			{
			printf("\n\nAll students: %d\nStudents with facilities: %d\nStudents without facilities: %d\n\n", 
				city.getUniversityStudents().size(), city.getUniversityStudentsWithFacilities().size(), city.getUniversityStudentsWithoutFacilities().size());
			int placesLeft = 0;
			for (auto hostelW : city.getAvailableHostels())
			{
				if (auto hostel = hostelW.lock())
				{
					placesLeft += hostel->getAvailablePlaces();
				}
			}
			printf("Settlemented students: %d\nStudents in queue: %d\nAvailable hostels places left: %d\n\n",
				city.getUniversityStudents().size() - city.getStudentsQueue().size(), city.getStudentsQueue().size(), placesLeft);
			printf("Available hostels:\n");
			for (auto hostelW : city.getAvailableHostels())
			{
				if (auto hostel = hostelW.lock())
				{
					printf("\t%d\n", hostel->getNumber());
				}
			}
			printf("\nDepartments with not settlemented students:\n");
			for (auto departmentW : city.getStudentsQueue().getDepartmentsInQueue())
			{
				if (auto department = departmentW.lock())
				{
					printf("\t%s\n", department->getName().c_str());
				}
			}
			printf("\n");
			system("pause");
			break;
			}
		case (2):
			{
			printf("\n\nUniversity students list:");
			for (auto studentW : city.getUniversityStudents())
			{
				if (auto const student = studentW.lock())
				{
					printStudentInfo(student->getName());
				}
			}
			system("pause");
			break;
			}
		case (3):
			{
			state = Main;
			break;
			}
		}
	}


	void UniversityCityCLI::printDepartmentHostels()
	{
		auto hostels = city.getDepartmentByName(curDepartmentName)->getHostels();
		for (auto hostelW : hostels)
		{
			if (auto hostel = hostelW.lock())
			{
				printf("  Number: %u;\tAll places: %d;\tAvailable places: %d;\tSettlemented students: %d (%d of this department)\n", hostel->getNumber(), hostel->getAllPlaces(), hostel->getAvailablePlaces(), hostel->getStudents().size(), hostel->getStudentsByDepartmentName(curDepartmentName).size());
			}
		}
	}

	void UniversityCityCLI::printDepartmentStudents()
	{
		auto students = city.getDepartmentByName(curDepartmentName)->getStudents();
		for (auto studentW : students)
		{
			if (auto student = studentW.lock())
			{
				printStudentInfo(student->getName());
			}
		}
	}

	void UniversityCityCLI::printStudentInfo(string name)
	{
		auto student = city.getStudentByName(name).lock();
		string st = "  Name:" + student->getName();
		st += student->hasFacilities() ? ";\tHas facilities" : "";
		if (auto hos = student->getHostel().lock())
			st += ";\tHostel" + hos->getNumber();
		if (city.getStudentsQueue().size() > 0)
		{
			st += city.getStudentsQueue().getSet().find(student) != city.getStudentsQueue().getSet().end() ?
				";\tIn queue" : "";
		}
		st += "\n";
		printf(st.c_str());
	}
	UniversityCityCLI::State UniversityCityCLI::menuChose(vector<State> states)
	{
		int res;
		string s;
		while (true)
		{
			printf(strings["Command"].c_str());
			cin >> s;
			try
			{
				res = atoi(s.c_str());
				if (res > 0 && res <= states.size())
					return states[res - 1];
				throw exception();
			}
			catch (...)
			{
				printf(strings["WrongCommand"].c_str());
			}
		}
	}

	int UniversityCityCLI::menuChose(int max)
	{
		int res;
		string s;
		while (true)
		{
			printf("\ncommand:");
			cin >> s;
			try
			{
				res = atoi(s.c_str());
				if (res > 0 && res <= max)
					return res;
				throw exception();
			}
			catch (...)
			{
				printf("Wrong command\n");
			}
		}
	}

	string UniversityCityCLI::getOperationString(vector<string> operations, bool writeHeader)
	{
		string res = writeHeader ? "Operations:\n" : "";
		for (int i = 1; i <= operations.size(); ++i)
		{
			res += "    " + to_string(i) + " - " + operations[i - 1] + "\n";
		}
		return res;
	}

	void UniversityCityCLI::fillRandomData()
	{
		random_device rd;  // "�������" ��������� �����, ��������� �������� ���������� 
		mt19937 gen(rd()); // ��������� ����� �������
		uniform_int_distribution<int> depC(3, 20), depStC(200, 1000), stFac(0, 9), depHosC(2, 4), hosAllPlC(210, 5000), hosAvPlC(30, 40);  // ����������� ���������� �������������
		int departmentsCount = depC(gen);
		queue<int> hostelsQ;
		for (int di = 1; di <= departmentsCount; ++di)
		{
			auto dep = make_shared<Department>("Department " + to_string(di));
			city.addDepartment(dep);
			int studentsCount = depStC(gen);
			for (int si = 1; si <= studentsCount; ++si)
			{
				bool const dontHasFacilities = stFac(gen);
				dep->addStudent(make_shared<Student>("Student " + to_string(si) + " d" + to_string(di), !dontHasFacilities));
			}
			int hostelsCount = depHosC(gen);
			while (hostelsQ.size() != 0 && hostelsCount != 0)
			{
				int curHostel = hostelsQ.front();
				hostelsQ.pop();
				hostelsCount--;
				dep->addHostel(city.getHostelByNumber(curHostel));
			}
			while (hostelsCount != 0)
			{
				int hostelAllPlaces = hosAllPlC(gen);
				int hostelAvailablePlaces = round((hostelAllPlaces / 100.0)*hosAvPlC(gen));
				auto hostel = make_shared<Hostel>(city.getHostels().size(), hostelAllPlaces, hostelAvailablePlaces);
				city.addHostel(hostel);
				hostelsQ.push(hostel->getNumber());
				if (hostelAvailablePlaces > 500)
					hostelsQ.push(hostel->getNumber());
				if (hostelAvailablePlaces > 1000)
					hostelsQ.push(hostel->getNumber());
				if (hostelAvailablePlaces > 1300)
					hostelsQ.push(hostel->getNumber());
				while (hostelsCount != 0 && hostelsQ.size() != 0)
				{
					int curHostel = hostelsQ.front();
					hostelsQ.pop();
					hostelsCount--;
					dep->addHostel(city.getHostelByNumber(curHostel));
				}
			}
		}
	}

	void UniversityCityCLI::run()
	{
		screenManager();
	}

	void UniversityCityCLI::runTest()
	{
		auto dep1 = make_shared<Department>("FICT");
		city.addDepartment(dep1);
		auto hos1 = make_shared<Hostel>(3, 210, 50);
		city.addHostel(hos1);
		dep1->addStudent(make_shared<Student>("Alexander Shkurpelo"));
		dep1->addStudents({ make_shared<Student>("Sakhniuk Anton"), make_shared<Student>("Dmitry Maksimchyk", true) });
		hos1->addDepartment(dep1);
		auto students = city.getUniversityStudents();
		city.organizeStudents();
		auto hos1After = city.getHostelByNumber(3);
		printf("%u\t%u\t%u\n\n", hos1After->getNumber(), hos1After->getAllPlaces(), hos1After->getAvailablePlaces());

		for (auto studentW : students)
		{
			auto student = studentW.lock();
			auto department = student->getDepartment().lock();
			auto hostel = student->getHostel().lock();
			printf("%s\t%s\t%u\n", student->getName().c_str(), department->getName().c_str(), hostel->getNumber());
		}
	}
}
