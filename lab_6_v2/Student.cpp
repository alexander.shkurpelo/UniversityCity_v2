#include "stdafx.h"
#include "Student.h"
#include "Department.h"
#include "DepartmentStudents.h"
#include "Hostel.h"

namespace UniversityCity
{
	Student::Student(const string& name, const bool& hasFacilities) : name_(name), hasFacilities_(hasFacilities) {}

	Student::~Student()
	{
		//(*hostel_.lock()).removeStudent(shared_from_this());
		//hostel_.reset();
		department_.reset();
	}

	string Student::getName() const
	{
		return this->name_;
	}

	bool Student::hasFacilities() const
	{
		return this->hasFacilities_;
	}

	Student::DepartmentWPtr Student::getDepartment() const
	{
		return this->department_;
	}

	Student::HostelWPtr Student::getHostel() const
	{
		return this->hostel_;
	}

	void Student::setFacilities(const bool& value)
	{
		this->hasFacilities_ = value;
	}

	void Student::changeDepartment(const DepartmentSPtr& newDepartment)
	{
		if (auto currentDep = this->department_.lock())
			(*currentDep).removeStudent(shared_from_this());
		this->department_ = newDepartment;
	}

	void Student::changeHostel(const HostelSPtr& newHostel)
	{
		/*if (auto currentHostel = hostel_.lock())
			(*currentHostel).removeStudent(shared_from_this());*/
		this->hostel_ = newHostel;
	}

	void Student::clearHostel()
	{
		/*if (auto currentHostel = hostel_.lock())
		(*currentHostel).removeStudent(shared_from_this());*/
		this->hostel_.reset();
	}
}
