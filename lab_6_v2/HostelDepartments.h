#pragma once
#include <unordered_set>
#include <memory>
#include "WeakPtrUtility.h"

using namespace std;

namespace UniversityCity
{
	class Department;

	class HostelDepartments
	{
	protected:
		typedef shared_ptr<Department> DepartmentSPtr;
		typedef weak_ptr<Department> DepartmentWPtr;
		typedef unordered_set<DepartmentWPtr, WeakPtrHash<Department>, WeakPtrEqual<Department>> Departments;

		Departments departments_;
	public:
		HostelDepartments();
		HostelDepartments(const DepartmentWPtr& department);
		HostelDepartments(const Departments& departments);
		virtual ~HostelDepartments();

		virtual bool addDepartment(const DepartmentWPtr& department);
		void addDepartments(const Departments& departments);
		virtual bool removeDepartment(const DepartmentWPtr& department);
		void removeAllDepartments();

		DepartmentWPtr getDepartmentByName(const string& number) const;
		Departments getDepartments() const;
	};
}
