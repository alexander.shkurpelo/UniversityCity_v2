#include "stdafx.h"
#include <algorithm>
#include <iterator>
#include "HostelStudents.h"
#include "Student.h"
#include "Department.h"

namespace UniversityCity
{
	HostelStudents::HostelStudents() : students_(Students()) {}

	HostelStudents::~HostelStudents() {}

	HostelStudents::StudentWPtr HostelStudents::addStudent(const StudentWPtr& student)
	{
		if (student.expired())
			return StudentWPtr();
		if (!students_.insert(student).second)
			return StudentWPtr();
		return student;
	}

	HostelStudents::Students HostelStudents::addStudents(const Students& students)
	{
		Students result;
		for (auto studentW : students)
		{
			if (!addStudent(studentW).expired())
				result.insert(studentW);
		}		
		return result;
	}

	HostelStudents::StudentWPtr HostelStudents::removeStudent(const StudentWPtr& student)
	{
		auto iter = students_.find(student.lock());
		if (iter == students_.end())
			return StudentWPtr();
		auto res = *iter;
		students_.erase(iter);
		return res;
	}

	HostelStudents::Students HostelStudents::removeAllStudents()
	{
		auto students = students_;
		students_.clear();
		return students;
	}

	HostelStudents::StudentWPtr HostelStudents::getStudentByName(const string& name) const
	{
		for (auto student : students_)
		{
			if (auto st = student.lock())
				if ((*st).getName() == name)
					return student;
		}
		return StudentWPtr();
	}

	HostelStudents::Students HostelStudents::getStudentsByDepartmentName(const string& name) const
	{
		Students res;
		for (auto studentW : students_)
		{
			if (auto st = studentW.lock())
				if (auto dep = st->getDepartment().lock())
					if ((*dep).getName() == name)
						res.insert(studentW);
		}
		return res;
	}

	HostelStudents::Students HostelStudents::getStudents() const
	{
		return students_;
	}

	HostelStudents::Students HostelStudents::getStudentsWithFacilities() const
	{
		Students result;
		for (auto studentW : students_)
		{
			if (auto st = studentW.lock())
				if (st->hasFacilities())
					result.insert(st);
		}
		return result;
	}

	HostelStudents::Students HostelStudents::getStudentsWithoutFacilities() const
	{
		Students result;
		for (auto studentW : students_)
		{
			if (auto st = studentW.lock())
				if (!st->hasFacilities())
					result.insert(st);
		}
		return result;
	}
}
