#include "stdafx.h"
#include "UniversityCity.h"
#include "UniversityCityInfo.h"
#include "Hostel.h"

namespace UniversityCity
{
	UniversityCity::UniversityCity() : UniversityCityData() {}

	UniversityCity::~UniversityCity() {}

	void UniversityCity::organizeDepartment(const DepartmentWPtr& departmentW)
	{
		if (departmentW.expired())
			return;
		const auto department = departmentW.lock();

		const auto hostelsIn = (*department).getHostels();
		const auto hostelsOut = getHostelsNotInDepartment(department);

		auto students = (*department).getStudentsWithFacilities();
		if (!settlementStudents(students, hostelsIn, hostelsOut))
			return;

		students = (*department).getStudentsWithoutFacilities();
		settlementStudents(students, hostelsIn);
	}

	bool UniversityCity::settlementStudents(const StudentsLink& students, const HostelsLink& hostelsIn, const HostelsLink& hostelsOut)
	{
		StudentsLink rest = students;
		for (auto hostels : { hostelsIn, hostelsOut })
		{
			for (auto hostelW : hostels)
			{
				if (hostelW.expired())
					continue;
				const auto hostel = hostelW.lock();
				rest = (*hostel).addStudents(rest);
				if (rest.empty())
					return true;
			}
		}
		studentsQueue_.push(rest);
		return false;
	}

	void UniversityCity::organizeStudents()
	{
		for (const auto department : departments_)
		{
			organizeDepartment(department);
		}
		auto const hostels = getAvailableHostels();
		if (studentsQueue_.size() == 0 || hostels.size() == 0)
			return;
		auto const students = studentsQueue_.getSet();
		studentsQueue_.popStudentsWithoutFacilities();
		studentsQueue_.popStudentsWithFacilities();
		settlementStudents(students, hostels);
	}

	StudentsQueue UniversityCity::getStudentsQueue() const
	{
		return studentsQueue_;
	}

	UniversityCityInfo UniversityCity::getInfoClass()
	{
		return UniversityCityInfo(this);
	}
}
