#include "stdafx.h"
#include "Hostel.h"
#include "Department.h"

namespace UniversityCity
{
	Hostel::Hostel(const size_t& number, const size_t& allPlaces) : number_(number), allPlaces_(allPlaces), availablePlaces_(allPlaces) {}

	Hostel::Hostel(const size_t& number, const size_t& allPlaces, const size_t availablePlaces) : number_(number), allPlaces_(allPlaces), availablePlaces_(availablePlaces) {}

	Hostel::~Hostel() {}

	size_t Hostel::getNumber() const { return number_; }

	size_t Hostel::getAllPlaces() const { return allPlaces_; }
	
	size_t Hostel::getAvailablePlaces() const { return availablePlaces_; }

	bool Hostel::addDepartment(const DepartmentWPtr& department)
	{
		if (!HostelDepartments::addDepartment(department))
			return false;
		(*department.lock()).addHostel(shared_from_this());
		return true;
	}

	bool Hostel::removeDepartment(const DepartmentWPtr& department)
	{
		if (!HostelDepartments::removeDepartment(department))
			return false;
		(*department.lock()).removeHostel(shared_from_this());
		return true;
	}

	HostelStudents::StudentWPtr Hostel::addStudent(const StudentWPtr& student)
	{
		if (availablePlaces_ == 0)
			return student;
		auto afterAdding = HostelStudents::addStudent(student);
		if (afterAdding.expired())
			return student;
		(*student.lock()).changeHostel(shared_from_this());
		availablePlaces_--;
		return StudentWPtr();
	}

	HostelStudents::StudentWPtr Hostel::removeStudent(const StudentWPtr& student)
	{
		auto deleted = HostelStudents::removeStudent(student);
		if (deleted.owner_before(student))
			return student;
		(*student.lock()).clearHostel();
		availablePlaces_++;
		return student;
	}
}
