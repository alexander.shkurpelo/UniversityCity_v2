#include "stdafx.h"
#include <algorithm>
#include "HostelSet.h"
#include  "Department.h"
#include <iterator>

namespace UniversityCity
{
	HostelSet::HostelSet() : hostels_(Hostels()) {}

	HostelSet::~HostelSet()
	{
		hostels_.clear();
	}

	bool HostelSet::addHostel(const HostelSPtr& Hostel)
	{
		if (Hostel == nullptr)
			return false;
		return hostels_.insert(Hostel).second;
	}

	void HostelSet::addHostels(const Hostels& Hostels)
	{
		hostels_.insert(Hostels.begin(), Hostels.end());
	}

	HostelSet::HostelSPtr HostelSet::getHostelByNumber(const size_t& number) const
	{
		auto res = find_if(hostels_.begin(), hostels_.end(), [number](const HostelSPtr& Hostel) {return (*Hostel).getNumber() == number; });
		if (res == hostels_.end())
			return nullptr;
		return *res;
	}

	HostelSet::Hostels HostelSet::getHostels() const
	{
		return hostels_;
	}

	HostelSet::HostelsLink HostelSet::getHostelsNotInDepartment(const weak_ptr<Department>& toIgnore) const
	{
		HostelsLink res;
		if (toIgnore.expired())
			return HostelsLink(hostels_.begin(), hostels_.end());
		copy_if(hostels_.begin(), hostels_.end(), inserter(res, res.begin()), [toIgnore](const HostelSPtr& hostel)
		{
			auto departments = (*hostel).getDepartments();
			return departments.find(toIgnore) == departments.end();
		});
		return res;
	}

	HostelSet::HostelsLink HostelSet::getAvailableHostels() const
	{
		HostelsLink res;
		copy_if(hostels_.begin(), hostels_.end(), inserter(res, res.begin()), 
				[](const HostelSPtr& hostel) { return hostel->getAvailablePlaces() != 0; });
		return res;
	}

	HostelSet::HostelsLink HostelSet::getNotAvailableHostels() const
	{
		HostelsLink res;
		copy_if(hostels_.begin(), hostels_.end(), inserter(res, res.begin()),
			[](const HostelSPtr& hostel) { return hostel->getAvailablePlaces() == 0; });
		return res;
	}

	HostelSet::HostelSPtr HostelSet::removeHostel(const HostelSPtr& Hostel)
	{
		auto iter = hostels_.find(Hostel);
		if (iter == hostels_.end())
			return nullptr;
		auto removed = *iter;
		hostels_.erase(iter);
		return removed;
	}

	HostelSet::HostelSPtr HostelSet::removeHostelByNumber(const size_t& number)
	{
		auto toRemove = getHostelByNumber(number);
		return removeHostel(toRemove);
	}

	HostelSet::Hostels HostelSet::removeAllHostels()
	{
		auto result = hostels_;
		hostels_.clear();
		return result;
	}
}
