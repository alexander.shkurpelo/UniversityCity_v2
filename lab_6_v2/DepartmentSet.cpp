#include "stdafx.h"
#include <algorithm>
#include "DepartmentSet.h"

namespace UniversityCity
{
	DepartmentSet::DepartmentSet() : departments_(Departments()) {}

	DepartmentSet::~DepartmentSet()
	{
		//departments_.clear();
	}

	bool DepartmentSet::addDepartment(const DepartmentSPtr& department)
	{
		if (department == nullptr)
			return false;
		return departments_.insert(department).second;
	}

	void DepartmentSet::addDepartments(const Departments& departments)
	{
		departments_.insert(departments.begin(), departments.end());
	}

	DepartmentSet::DepartmentSPtr DepartmentSet::getDepartmentByName(const string& name) const
	{
		auto res = find_if(departments_.begin(), departments_.end(), [name](const DepartmentSPtr& department) {return (*department).getName() == name; });
		if (res == departments_.end())
			return nullptr;
		return *res;
	}

	DepartmentSet::Departments DepartmentSet::getDepartments() const
	{
		return departments_;
	}

	DepartmentSet::DepartmentSPtr DepartmentSet::removeDepartment(const DepartmentSPtr& department)
	{
		auto iter = departments_.find(department);
		if (iter == departments_.end())
			return nullptr;
		auto removed = *iter;
		departments_.erase(iter);
		return removed;
	}

	DepartmentSet::DepartmentSPtr DepartmentSet::removeDepartmentByName(const string& name)
	{
		auto toRemove = getDepartmentByName(name);
		return removeDepartment(toRemove);
	}

	DepartmentSet::Departments DepartmentSet::removeAllDepartments()
	{
		auto result = departments_;
		departments_.clear();
		return result;
	}
}
