#pragma once
#include <memory>
#include <unordered_set>
#include "WeakPtrUtility.h"
#include "Student.h"

using namespace std;

namespace UniversityCity
{
	class HostelStudents
	{
	protected:
		//typedef shared_ptr<Student> StudentSPtr;
		//typedef weak_ptr<Student> StudentWPtr;
		//typedef unordered_set<StudentWPtr, WeakPtrHash<Student>, WeakPtrEqual<Student>> Students;

		StudentsLink students_;
	public:
		HostelStudents();
		virtual ~HostelStudents();

		virtual StudentWPtr addStudent(const StudentWPtr& student);
		Students addStudents(const Students& students);
		virtual StudentWPtr removeStudent(const StudentWPtr& student);
		Students removeAllStudents();

		StudentWPtr getStudentByName(const string& name) const;
		Students getStudentsByDepartmentName(const string& name) const;
		Students getStudents() const;
		Students getStudentsWithFacilities() const;
		Students getStudentsWithoutFacilities() const;
	};
}

