#include "stdafx.h"
#include <algorithm>
#include <iterator>
#include "DepartmentStudents.h"
#include "Student.h"

namespace UniversityCity
{
	DepartmentStudents::DepartmentStudents() : students_(Students()) {}

	DepartmentStudents::~DepartmentStudents() {}

	bool DepartmentStudents::addStudent(const StudentSPtr& student)
	{
		if (student == nullptr)
			return false;
		return students_.insert(student).second;
	}

	void DepartmentStudents::addStudents(const Students& students)
	{
		for_each(students.begin(), students.end(), [this](const StudentSPtr& student) { addStudent(student); });
	}

	DepartmentStudents::StudentSPtr DepartmentStudents::removeStudent(const StudentWPtr& student)
	{
		auto iter = students_.find(student.lock());
		if (iter == students_.end())
			return nullptr;
		auto res = *iter;
		students_.erase(iter);
		return res;
	}

	DepartmentStudents::Students DepartmentStudents::removeAllStudents()
	{
		auto students = students_;
		students_.clear();
		return students;
	}

	DepartmentStudents::StudentWPtr DepartmentStudents::getStudentByName(const string& name) const
	{
		auto res = find_if(students_.begin(), students_.end(), [name](const StudentSPtr& student) { return (*student).getName() == name; });
		if (res == students_.end())
			return StudentWPtr();
		return *res;
	}

	DepartmentStudents::StudentsLink DepartmentStudents::getStudents() const
	{
		StudentsLink result;
		result.insert(students_.begin(), students_.end());
		return result;
	}

	DepartmentStudents::StudentsLink DepartmentStudents::getStudentsWithFacilities() const
	{
		StudentsLink result;
		copy_if(students_.begin(), students_.end(), inserter(result, result.begin()), [](const StudentSPtr& student) { return student->hasFacilities(); });
		return result;
	}

	DepartmentStudents::StudentsLink DepartmentStudents::getStudentsWithoutFacilities() const
	{
		StudentsLink result;
		copy_if(students_.begin(), students_.end(), inserter(result, result.begin()), [](const StudentSPtr& student) { return !student->hasFacilities(); });
		return result;
	}
}
