#pragma once
#include "Hostel.h"

using namespace std;

namespace UniversityCity
{
	class HostelSet
	{
	protected:
		typedef shared_ptr<Hostel> HostelSPtr;
		typedef weak_ptr<Hostel> HostelWPtr;
		typedef unordered_set<HostelSPtr> Hostels;

		Hostels hostels_;
	public:
		typedef unordered_set<HostelWPtr, WeakPtrHash<Hostel>, WeakPtrEqual<Hostel>> HostelsLink;

		HostelSet();
		virtual ~HostelSet();

		bool addHostel(const HostelSPtr& Hostel);
		void addHostels(const Hostels& Hostels);

		HostelSPtr getHostelByNumber(const size_t& number) const;
		Hostels getHostels() const;
		HostelsLink getHostelsNotInDepartment(const weak_ptr<Department>& toIgnore) const;
		HostelsLink getAvailableHostels() const;
		HostelsLink getNotAvailableHostels() const;

		HostelSPtr removeHostel(const HostelSPtr& Hostel);
		HostelSPtr removeHostelByNumber(const size_t& number);
		Hostels removeAllHostels();
	};
}
