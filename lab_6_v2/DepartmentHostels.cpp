#include "stdafx.h"
#include <algorithm>
#include "DepartmentHostels.h"
#include "Hostel.h"
#include <iterator>

namespace UniversityCity
{
	DepartmentHostels::DepartmentHostels() : hostels_(Hostels()) {}

	DepartmentHostels::DepartmentHostels(const HostelWPtr& hostel) : hostels_({hostel}) {}

	DepartmentHostels::DepartmentHostels(const Hostels& hostels) : hostels_(hostels) {}

	DepartmentHostels::~DepartmentHostels() {}

	bool DepartmentHostels::addHostel(const HostelWPtr& hostel)
	{
		if (hostel.expired())
			return false;
		return this->hostels_.insert(hostel).second;
	}

	void DepartmentHostels::addHostels(const Hostels& hostels)
	{
		for_each(hostels.begin(), hostels.end(), [this](const HostelWPtr& hostel) { addHostel(hostel); });
	}

	bool DepartmentHostels::removeHostel(const HostelWPtr& hostel)
	{
		auto toRemove = this->hostels_.find(hostel);
		if (toRemove == this->hostels_.end())
			return false;
		this->hostels_.erase(toRemove);
		return true;
	}

	void DepartmentHostels::removeAllHostels()
	{
		for_each(hostels_.begin(), hostels_.end(), [this](const HostelWPtr& hostel) { removeHostel(hostel); });
	}

	DepartmentHostels::HostelWPtr DepartmentHostels::getHostelByNumber(const size_t& number) const
	{
		auto res = find_if(hostels_.begin(), hostels_.end(), [number](const HostelWPtr& department) { return (*department.lock()).getNumber() == number; });
		if (res == hostels_.end())
			return HostelWPtr();
		return *res;
	}

	DepartmentHostels::Hostels DepartmentHostels::getHostels() const
	{
		return hostels_;
	}

	DepartmentHostels::Hostels DepartmentHostels::getAvailableHostels() const
	{
		Hostels res;
		copy_if(hostels_.begin(), hostels_.end(), inserter(res, res.begin()), [](const HostelWPtr& hostel)
		{
			if (auto hos = hostel.lock())
				if (hos->getAvailablePlaces() != 0)
					return true;
			return false;
		});
		return res;
	}

	DepartmentHostels::Hostels DepartmentHostels::getNotAvailableHostels() const
	{
		Hostels res;
		copy_if(hostels_.begin(), hostels_.end(), inserter(res, res.begin()), [](const HostelWPtr& hostel)
		{
			if (auto hos = hostel.lock())
				if (hos->getAvailablePlaces() == 0)
					return true;
			return false;
		});
		return res;
	}
}
