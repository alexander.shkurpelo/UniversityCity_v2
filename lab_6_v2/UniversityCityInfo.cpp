#include "stdafx.h"
#include "UniversityCityInfo.h"
#include "UniversityCity.h"

namespace UniversityCity
{
	UniversityCityInfo::UniversityCityInfo() {}

	UniversityCityInfo::UniversityCityInfo(UniversityCity* universityCity) : uCity(universityCity) {}

	UniversityCityInfo::~UniversityCityInfo()
	{
		
	}

	int UniversityCityInfo::allStudentsCount() const
	{
		return uCity->getUniversityStudents().size();
	}

	int UniversityCityInfo::studentsWithFacilitiesCount() const
	{
		return uCity->getUniversityStudentsWithFacilities().size();
	}

	int UniversityCityInfo::studentsWithoutFacilitiesCount() const
	{
		return uCity->getUniversityStudentsWithoutFacilities().size();
	}

	int UniversityCityInfo::departmentsCount() const
	{
		return uCity->getDepartments().size();
	}

	int UniversityCityInfo::hostelsCount() const
	{
		return uCity->getHostels().size();
	}

	int UniversityCityInfo::availableHostelsCount() const
	{
		return uCity->getAvailableHostels().size();
	}

	int UniversityCityInfo::notAvailableHostelsCount() const
	{
		return uCity->getNotAvailableHostels().size();
	}

	int UniversityCityInfo::allHostelsPlaces() const
	{
		int res = 0;
		auto hostels = uCity->getHostels();
		for (auto hostel : hostels)
		{
			res += hostel->getAllPlaces();
		}
		return res;
	}

	int UniversityCityInfo::allHostelsAvailablePlaces() const
	{
		int res = 0;
		auto hostels = uCity->getHostels();
		for (auto hostel : hostels)
		{
			res += hostel->getAvailablePlaces();
		}
		return res;
	}

	int UniversityCityInfo::settlemetedStudentsCount() const
	{
		int res = 0;
		auto hostels = uCity->getHostels();
		for (auto hostel : hostels)
		{
			res += hostel->getStudents().size();
		}
		return res;
	}

	int UniversityCityInfo::studentsInQueueCount() const
	{
		return uCity->getStudentsQueue().size();
	}

	int UniversityCityInfo::hostelsCount(string departmentName) const
	{
		return uCity->getDepartmentByName(departmentName)->getHostels().size();
	}

	int UniversityCityInfo::availableHostelsCount(string departmentName) const
	{
		return uCity->getDepartmentByName(departmentName)->getAvailableHostels().size();
	}

	int UniversityCityInfo::notAvailableHostelsCount(string departmentName) const
	{
		return uCity->getDepartmentByName(departmentName)->getNotAvailableHostels().size();
	}

	int UniversityCityInfo::allStudentsCount(string departmentName) const
	{
		return uCity->getDepartmentByName(departmentName)->getStudents().size();
	}

	int UniversityCityInfo::allHostelsPlaces(string departmentName) const
	{
		int res = 0;
		auto hostels = uCity->getDepartmentByName(departmentName)->getHostels();
		for (auto hostelW : hostels)
		{
			if (auto const hostel = hostelW.lock())
				res += hostel->getAllPlaces();
		}
		return res;
	}

	int UniversityCityInfo::allHostelsAvailablePlaces(string departmentName) const
	{
		int res = 0;
		auto hostels = uCity->getDepartmentByName(departmentName)->getHostels();
		for (auto hostelW : hostels)
		{
			if (auto const hostel = hostelW.lock())
				res += hostel->getAvailablePlaces();
		}
		return res;
	}

	int UniversityCityInfo::settlemetedStudentsCount(string departmentName) const
	{
		int res = 0;
		auto hostels = uCity->getDepartmentByName(departmentName)->getHostels();
		for (auto hostelW : hostels)
		{
			if (auto const hostel = hostelW.lock())
				res += hostel->getStudents().size();
		}
		return res;
	}

	int UniversityCityInfo::studentsInQueueCount(string departmentName) const
	{
		auto const department = uCity->getDepartmentByName(departmentName);
		return uCity->getStudentsQueue().getDepartmentQueue(department).size();
	}

	int UniversityCityInfo::departmentsCount(int hostelsNumber) const
	{
		return uCity->getHostelByNumber(hostelsNumber)->getDepartments().size();
	}

	int UniversityCityInfo::allStudentsCount(int hostelsNumber) const
	{
		return uCity->getHostelByNumber(hostelsNumber)->getStudents().size();
	}

	int UniversityCityInfo::allHostelsPlaces(int hostelsNumber) const
	{
		return uCity->getHostelByNumber(hostelsNumber)->getAllPlaces();
	}

	int UniversityCityInfo::allHostelsAvailablePlaces(int hostelsNumber) const
	{
		return uCity->getHostelByNumber(hostelsNumber)->getAvailablePlaces();
	}

	void UniversityCityInfo::reset()
	{
	}
}
