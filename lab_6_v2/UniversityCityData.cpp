#include "stdafx.h"
#include "UniversityCityData.h"
#include "Department.h"
#include "Student.h"

namespace UniversityCity
{
	UniversityCityData::UniversityCityData() : DepartmentSet(), HostelSet() {}

	UniversityCityData::~UniversityCityData()
	{
		departments_.clear();
		hostels_.clear();
	}

	UniversityCityData::StudentWPtr UniversityCityData::getStudentByName(const string& name) const
	{
		for (const auto department : departments_)
		{
			if (auto res = (*department).getStudentByName(name).lock())
				return res;;
		}
		return StudentSPtr();
	}

	UniversityCityData::StudentsLink UniversityCityData::getUniversityStudents() const
	{
		StudentsLink result;
		for (const auto department : departments_)
		{
			auto toAdd = (*department).getStudents();
			result.insert(toAdd.begin(), toAdd.end());
		}
		return result;
	}

	UniversityCityData::StudentsLink UniversityCityData::getUniversityStudentsWithFacilities() const
	{
		StudentsLink result;
		for (const auto department : departments_)
		{
			auto toAdd = (*department).getStudentsWithFacilities();
			result.insert(toAdd.begin(), toAdd.end());
		}
		return result;
	}

	UniversityCityData::StudentsLink UniversityCityData::getUniversityStudentsWithoutFacilities() const
	{
		StudentsLink result;
		for (const auto department : departments_)
		{
			auto toAdd = (*department).getStudentsWithoutFacilities();
			result.insert(toAdd.begin(), toAdd.end());
		}
		return result;
	}
}
