#pragma once
#include "StudentsQueue.h"
#include "UniversityCityData.h"

namespace UniversityCity
{
	class UniversityCityInfo;

	class UniversityCity;
	typedef weak_ptr<UniversityCity> UCityWPtr;
	typedef shared_ptr<UniversityCity> UCitySPtr;

	class UniversityCity : public UniversityCityData, enable_shared_from_this<UniversityCity>
	{
	protected:
		StudentsQueue studentsQueue_;

		void organizeDepartment(const DepartmentWPtr&);
		bool settlementStudents(const StudentsLink&, const HostelsLink& hostelsIn, const HostelsLink& hostelsOut = {});
	public:
		UniversityCity();
		~UniversityCity();

		void organizeStudents();
		
		StudentsQueue getStudentsQueue() const;
		UniversityCityInfo getInfoClass();
	};

	
}

