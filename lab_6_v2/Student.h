#pragma once
#include <string>
#include <memory>
#include "Department.h"
#include "Hostel.h"

using namespace std;

namespace UniversityCity
{
	class Student;
	typedef shared_ptr<Student> StudentSPtr;
	typedef weak_ptr<Student> StudentWPtr;
	typedef unordered_set<StudentSPtr> Students;
	typedef unordered_set<StudentWPtr, WeakPtrHash<Student>, WeakPtrEqual<Student>> StudentsLink;

	class Student : public enable_shared_from_this<Student>
	{
		friend class Department;
		friend class Hostel;
		friend class UnivercityCityCLI;

		//typedef weak_ptr<Department> DepartmentWPtr;
		//typedef shared_ptr<Department> DepartmentSPtr;
		//typedef weak_ptr<Hostel> HostelWPtr;
		//typedef shared_ptr<Hostel> HostelSPtr;

		string name_;
		bool hasFacilities_;
		DepartmentWPtr department_;
		HostelWPtr hostel_;

		void changeDepartment(const DepartmentSPtr& newDepartment);
		void changeHostel(const HostelSPtr& newHostel);
		void clearHostel();
	public:
		Student(const string& name, const bool& hasFacilities = false);
		~Student();

		string getName() const;
		bool hasFacilities() const;
		DepartmentWPtr getDepartment() const;
		HostelWPtr getHostel() const;

		void setFacilities(const bool& value);
	};


};

