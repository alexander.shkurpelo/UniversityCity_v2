#pragma once
template<class... Classes>
class ClassConnector : public Classes...
{
public:
	ClassConnector() {}
	ClassConnector(const Classes&&... classes) : Classes(classes)... {}
};
