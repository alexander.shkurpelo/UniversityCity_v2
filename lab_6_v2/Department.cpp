#include "stdafx.h"
#include "Department.h"
#include "Student.h"
#include "Hostel.h"
#include <algorithm>
#include <iterator>

namespace UniversityCity
{
	Department::Department(const string& name) : name_(name) {}

	Department::~Department()
	{
		//students_.clear();
		//hostels_.clear();
	}

	string Department::getName() const { return name_; }

	bool Department::addStudent(const StudentSPtr& student)
	{
		if (student == nullptr)// || !getStudentByName(student->getName()).expired())
			return false;
		(*student).changeDepartment(shared_from_this());
		return students_.insert(student).second;
	}

	bool Department::addHostel(const HostelWPtr& hostel)
	{
		if (!DepartmentHostels::addHostel(hostel))
			return false;
		(*hostel.lock()).addDepartment(shared_from_this());
		return true;
	}

	bool Department::removeHostel(const HostelWPtr& hostel)
	{
		if (!DepartmentHostels::removeHostel(hostel))
			return false;
		(*hostel.lock()).removeDepartment(shared_from_this());
		return true;
	}
}
