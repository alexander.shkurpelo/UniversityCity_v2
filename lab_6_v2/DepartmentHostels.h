#pragma once
#include <memory>
#include <unordered_set>
#include "WeakPtrUtility.h"
#include "Hostel.h"

using namespace std;

namespace UniversityCity
{
	class DepartmentHostels
	{
	protected:
		HostelsLink hostels_;
	public:
		DepartmentHostels();
		DepartmentHostels(const HostelWPtr& hostel);
		DepartmentHostels(const HostelsLink& hostels);
		virtual ~DepartmentHostels();

		virtual bool addHostel(const HostelWPtr& hostel);
		void addHostels(const HostelsLink& hostels);
		virtual bool removeHostel(const HostelWPtr& hostel);
		void removeAllHostels();

		HostelWPtr getHostelByNumber(const size_t& number) const;
		Hostels getHostels() const;
		Hostels getAvailableHostels() const;
		Hostels getNotAvailableHostels() const;
	};
}

