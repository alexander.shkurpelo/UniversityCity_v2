#include "stdafx.h"
#include "StudentSet.h"
#include "Student.h"

namespace UniversityCity
{
	template <class T>
	StudentSet<T, typename enable_if<is_base_of_v<_Ptr_base<Student>, T>>::type>::
		StudentSet() {}

	template <class T>
	StudentSet<T, typename enable_if<is_base_of_v<_Ptr_base<Student>, T>>::type>::
		StudentSet(const T& student) : students_({ student }) {}

	template <class T>
	StudentSet<T, typename enable_if<is_base_of_v<_Ptr_base<Student>, T>>::type>::
		StudentSet(const Students& students) : students_(students) {}

	template <class T>
	T StudentSet<T, typename enable_if<is_base_of_v<_Ptr_base<Student>, T>>::type>::addStudent(const T& student)
	{
		this->_students.insert(student);
		return T();
	}

}
