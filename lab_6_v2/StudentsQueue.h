#pragma once
#include <unordered_set>
#include "Department.h"

using namespace std;

namespace UniversityCity
{
	class StudentsQueue
	{
	protected:
		typedef weak_ptr<Department> DepartmentWPtr;
		typedef unordered_set<DepartmentWPtr, WeakPtrHash<Department>, WeakPtrEqual<Department>> Departments;
		typedef weak_ptr<Student> StudentWPtr;
		typedef weak_ptr<Hostel> HostelWPtr;
		typedef unordered_set<StudentWPtr, WeakPtrHash<Student>, WeakPtrEqual<Student>> Students;
		typedef vector<StudentWPtr> SQueue;
		
		SQueue studentsQueue_;
	public:
		StudentsQueue();
		StudentsQueue(const SQueue& studentsQueue);
		StudentsQueue(const Students& students);
		~StudentsQueue();

		void push(const StudentWPtr& student);
		void push(const Students& students);
		StudentWPtr back() const;
		StudentWPtr front() const;
		StudentWPtr pop();
		bool empty() const;
		size_t size() const;

		Students getSet() const;
		SQueue getVector() const;
		Students popStudentsWithFacilities();
		Students popStudentsWithoutFacilities();
		Students getStudentsWithFacilities() const;
		Students getStudentsWithoutFacilities() const;

		SQueue getDepartmentQueue(const DepartmentWPtr& department) const;
		Departments getDepartmentsInQueue() const;
	};
}

