#pragma once
#include <memory>
#include <string>

using namespace std;

namespace UniversityCity
{
	class UniversityCity;

	class UniversityCityInfo
	{
		UniversityCity* uCity;
	public:
		UniversityCityInfo();
		UniversityCityInfo(UniversityCity* universityCity);
		~UniversityCityInfo();

		int allStudentsCount() const;
		int studentsWithFacilitiesCount() const;
		int studentsWithoutFacilitiesCount() const;
		int departmentsCount() const;
		int hostelsCount() const;
		int availableHostelsCount() const;
		int notAvailableHostelsCount() const;
		int allHostelsPlaces() const;
		int allHostelsAvailablePlaces() const;
		int settlemetedStudentsCount() const;
		int studentsInQueueCount() const;

		int hostelsCount(string departmentName) const;
		int availableHostelsCount(string departmentName) const;
		int notAvailableHostelsCount(string departmentName) const;
		int allStudentsCount(string departmentName) const;
		int allHostelsPlaces(string departmentName) const;
		int allHostelsAvailablePlaces(string departmentName) const;
		int settlemetedStudentsCount(string departmentName) const;
		int studentsInQueueCount(string departmentName) const;

		int departmentsCount(int hostelsNumber) const;
		int allStudentsCount(int hostelsNumber) const;
		int allHostelsPlaces(int hostelsNumber) const;
		int allHostelsAvailablePlaces(int hostelsNumber) const;

		void reset();
	};
}

