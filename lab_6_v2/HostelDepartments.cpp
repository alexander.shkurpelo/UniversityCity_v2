#include "stdafx.h"
#include <algorithm>
#include "HostelDepartments.h"
#include "Department.h"

namespace UniversityCity
{
	HostelDepartments::HostelDepartments() : departments_(Departments()) {}

	HostelDepartments::HostelDepartments(const DepartmentWPtr& department) : departments_({department}) {}

	HostelDepartments::HostelDepartments(const Departments& departments) : departments_(departments) {}

	HostelDepartments::~HostelDepartments() {}

	bool HostelDepartments::addDepartment(const DepartmentWPtr& department)
	{
		if (department.expired())
			return false;
		return departments_.insert(department).second;
	}

	void HostelDepartments::addDepartments(const Departments& departments)
	{
		for_each(departments.begin(), departments.end(), [this](const DepartmentWPtr& department) { addDepartment(department); });
	}

	bool HostelDepartments::removeDepartment(const DepartmentWPtr& department)
	{
		auto toRemove = this->departments_.find(department);
		if (toRemove == this->departments_.end())
			return false;
		this->departments_.erase(toRemove);
		return true;
	}

	void HostelDepartments::removeAllDepartments()
	{
		for_each(departments_.begin(), departments_.end(), [this](const DepartmentWPtr& department) { removeDepartment(department); });
	}

	HostelDepartments::DepartmentWPtr HostelDepartments::getDepartmentByName(const string& name) const
	{
		auto res = find_if(departments_.begin(), departments_.end(), [name](const DepartmentWPtr& department) { return (*department.lock()).getName() == name; });
		if (res == departments_.end())
			return DepartmentWPtr();
		return *res;
	}

	HostelDepartments::Departments HostelDepartments::getDepartments() const
	{
		return departments_;
	}
}
