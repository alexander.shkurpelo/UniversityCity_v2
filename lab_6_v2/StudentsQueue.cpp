#include "stdafx.h"
#include "StudentsQueue.h"
#include <algorithm>
#include <iterator>

//#include "Department.h"

namespace UniversityCity
{
	StudentsQueue::StudentsQueue() {}

	StudentsQueue::StudentsQueue(const SQueue& studentsQueue) : studentsQueue_(studentsQueue) {}

	StudentsQueue::StudentsQueue(const Students& students) : studentsQueue_(students.begin(), students.end()) {}

	StudentsQueue::~StudentsQueue()
	{
		//studentsQueue_.clear();
	}

	StudentsQueue::Students StudentsQueue::getSet() const
	{
		if (empty())
			return Students();
		return Students(studentsQueue_.begin(), studentsQueue_.end());
	}

	vector<StudentsQueue::StudentWPtr> StudentsQueue::getVector() const
	{
		return studentsQueue_;
	}

	StudentsQueue::Students StudentsQueue::popStudentsWithFacilities()
	{
		auto res = getStudentsWithFacilities();
		auto newQueue = getStudentsWithoutFacilities();
		studentsQueue_ = SQueue(newQueue.begin(), newQueue.end());
		return res;
	}

	StudentsQueue::Students StudentsQueue::popStudentsWithoutFacilities()
	{
		auto res = getStudentsWithoutFacilities();		
		auto newQueue = getStudentsWithFacilities();
		studentsQueue_ = SQueue(newQueue.begin(), newQueue.end());
		return res;
	}

	StudentsQueue::Students StudentsQueue::getStudentsWithFacilities() const
	{
		Students res;
		copy_if(studentsQueue_.begin(), studentsQueue_.end(), inserter(res, res.begin()), [](const StudentWPtr& studentW)
		{
			if (studentW.expired())
				return false;
			return studentW.lock()->hasFacilities();
		});
		return res;
	}

	StudentsQueue::Students StudentsQueue::getStudentsWithoutFacilities() const
	{
		Students res;
		copy_if(studentsQueue_.begin(), studentsQueue_.end(), inserter(res, res.begin()), [](const StudentWPtr& studentW)
		{
			if (studentW.expired())
				return false;
			return !studentW.lock()->hasFacilities();
		});
		return res;
	}

	StudentsQueue::SQueue StudentsQueue::getDepartmentQueue(const DepartmentWPtr& department) const
	{
		if (department.expired() || studentsQueue_.empty())
			return SQueue();

		SQueue res,
			   curQueue = studentsQueue_;
		string departmentName = (*department.lock()).getName();

		while (!curQueue.empty())
		{
			auto student = curQueue.front();
			if (auto st = student.lock())
				if (auto dep = (*st).getDepartment().lock())
					if ((*dep).getName() == departmentName)
						res.push_back(student);
			curQueue.erase(curQueue.begin());
		}
		return res;
	}

	StudentsQueue::Departments StudentsQueue::getDepartmentsInQueue() const
	{
		Departments res;
		for (auto student : studentsQueue_)
		{
			if (auto st = student.lock())
				res.insert(st->getDepartment());
		}
		return res;
	}

	void StudentsQueue::push(const StudentWPtr& student)
	{
		if (student.expired())
			return;
		studentsQueue_.push_back(student);
	}

	void StudentsQueue::push(const Students& students)
	{
		for (auto student : students)
		{
			push(student);
		}
	}

	StudentsQueue::StudentWPtr StudentsQueue::back() const
	{
		if (empty())
			return StudentWPtr();
		return studentsQueue_.back();
	}

	StudentsQueue::StudentWPtr StudentsQueue::front() const
	{
		if (empty())
			return StudentWPtr();
		return studentsQueue_.front();
	}

	StudentsQueue::StudentWPtr StudentsQueue::pop()
	{
		if (empty())
			return StudentWPtr();
		auto removed = studentsQueue_.front();
		studentsQueue_.erase(studentsQueue_.begin());
		return removed;
	}

	bool StudentsQueue::empty() const
	{
		return studentsQueue_.empty();
	}

	size_t StudentsQueue::size() const
	{
		return studentsQueue_.size();
	}
}
