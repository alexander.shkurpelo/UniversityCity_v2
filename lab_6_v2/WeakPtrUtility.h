#pragma once
#include <memory>

template<typename T>
struct WeakPtrHash : public std::unary_function<std::weak_ptr<T>, size_t> {
	size_t operator()(const std::weak_ptr<T>& wp) const
	{
		auto sp = wp.lock();
		return std::hash<decltype(sp)>()(sp);
	}
};

template<typename T>
struct WeakPtrEqual : public std::unary_function<std::weak_ptr<T>, bool> {

	bool operator()(const std::weak_ptr<T>& left, const std::weak_ptr<T>& right) const
	{
		return !left.owner_before(right) && !right.owner_before(left);
	}
};

