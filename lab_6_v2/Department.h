#pragma once
#include <string>
#include <memory>
#include <unordered_set>
#include "DepartmentStudents.h"
#include "DepartmentHostels.h"

using namespace std;

namespace UniversityCity
{
	class Department;
	typedef shared_ptr<Department> DepartmentSPtr;
	typedef weak_ptr<Department> DepartmentWPtr;
	typedef unordered_set<DepartmentSPtr> Departments;
	typedef unordered_set<DepartmentWPtr, WeakPtrHash<Department>, WeakPtrEqual<Department>> DepartmentsLink;

	class Department : public DepartmentStudents, public DepartmentHostels, public enable_shared_from_this<Department>
	{
		string name_;
	public:
		Department(const string& name);
		~Department();
		
		string getName() const;

		bool addStudent(const StudentSPtr& student) override;

		bool addHostel(const HostelWPtr& hostel) override;
		bool removeHostel(const HostelWPtr& hostel) override;
	};
}
