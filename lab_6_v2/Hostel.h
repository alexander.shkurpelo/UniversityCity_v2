#pragma once
#include <memory>
#include "Student.h"
#include "HostelDepartments.h"
#include "HostelStudents.h"

using namespace std;

namespace UniversityCity
{
	class Hostel;
	typedef weak_ptr<Hostel> HostelWPtr;
	typedef shared_ptr<Hostel> HostelSPtr;
	typedef unordered_set<HostelSPtr> Hostels;
	typedef unordered_set<HostelWPtr, WeakPtrHash<Hostel>, WeakPtrEqual<Hostel>> HostelsLink;

	class Hostel : public HostelDepartments, public HostelStudents, public enable_shared_from_this<Hostel>
	{
		size_t number_, allPlaces_, availablePlaces_;
	public:
		Hostel(const size_t& number, const size_t& allPlaces);
		Hostel(const size_t& number, const size_t& allPlaces, const size_t availablePlaces);
		~Hostel();

		size_t getNumber() const;
		size_t getAllPlaces() const;
		size_t getAvailablePlaces() const;

		bool addDepartment(const DepartmentWPtr& department) override;
		bool removeDepartment(const DepartmentWPtr& department) override;

		StudentWPtr addStudent(const StudentWPtr& student) override;
		StudentWPtr removeStudent(const StudentWPtr& student) override;
	};
}

