#pragma once
#include "UniversityCity.h"
#include <unordered_map>

using namespace std;

namespace UniversityCity
{
	class UniversityCityCLI
	{
	private:
		enum State
		{
			Intro,
			Main,
			AddDepartment,
			AddedDepartment,
			AddHostel,
			AddedHostel,
			AddStudent,
			ChoseStudent,
			ChosenStudent,
			ChoseDepartment,
			ChosenDepartment,
			ManageDepartment,
			ManageHostel,
			DeleteDepartment,
			InfoDepartment,
			InfoHostel,
			InfoStudent,
			ChoseHostel,
			ChosenHostel,
			Info,
			MakeSettlement,
			Exit
		} state;

		unordered_map<string, string> strings;
		string curDepartmentName, curStudentName;
		size_t curHostelNumber;

		UniversityCity city;
		UniversityCityInfo cityInfo;

		void screenManager();

		void initStrings();
		void printDepartments();
		void printHostels();
		void printDepartmentHostels();
		void printDepartmentStudents();
		void printDepartmentHeaderInfo();

		void introScreen();
		void mainMenuScreen();
		void addDepartmentScreen();
		void addedDepartmentScreen();
		void choseDepartmentScreen();
		void chosenDepartmentScreen();
		void manageDepartmentScreen();
		void infoDepartmentScreen();
		void addStudentScreen();
		void choseStudentScreen();
		void addHostelScreen();
		void addedHostelScreen();
		void choseHostelScreen();
		void chosenHostelScreen();
		void infoHostelScreen();
		void chosenStudentScreen();
		void infoScreen();
		void makeSettlement();

		void printStudentInfo(string name);

		State menuChose(vector<State> states);
		int menuChose(int max);

		string getOperationString(vector<string> operations, bool writeHeader = true);

		void fillRandomData();
	public:
		UniversityCityCLI();
		~UniversityCityCLI();

		void run();
		void runTest();
	};
}
