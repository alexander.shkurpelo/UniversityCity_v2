#pragma once
#include <memory>
#include <unordered_set>
#include "WeakPtrUtility.h"
//#include "Department.h"

using namespace std;

//class Student;
namespace UniversityCity
{
	class Student;
	typedef weak_ptr<Student> StudentWPtr;
	typedef shared_ptr<Student> StudentSPtr;

	template<class T, class Enable = void> class StudentSet;

	template<class T>
	class StudentSet<T, typename enable_if<is_base_of_v<_Ptr_base<Student>, T>>::type>  //(is_same<StudentSPtr, T>::value || is_same<StudentsWPtr, T>::value)>::type>
	{
	protected:
		typedef unordered_set<T, typename enable_if<is_same<StudentWPtr, T>::value, WeakPtrHash<Student>::result_type>::type,
								 typename enable_if<is_same<StudentWPtr, T>::value, WeakPtrEqual<Student>::result_type>::type> Students;
		//typedef unordered_set<weak_ptr<Student>, WeakPtrHash<Student>, WeakPtrEqual<Student>> Students;

		Students students_;
	public:
		StudentSet();
		StudentSet(const T& student);
		StudentSet(const Students& students);
		//~StudentSet();

		T addStudent(const T& student);
		//Students addStudents(const Students& students);
		//bool removeStudent(const T& student);
		//void removeAllStudents();

		//T getStudentByName(const stirng& name);
		//Students getStudents() const;
		//Students getStudentsWithFacilities() const;
		//Students getStudentsWithoutFacilities() const;
	};

}

